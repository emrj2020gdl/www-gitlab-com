---
layout: handbook-page-toc
title: Real-time Editing of Issue Descriptions (REID) Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## The Real-time Editing of Issue Descriptions (REID) Single-Engineer Group

The Real-time Editing of Issue Descriptions (REID) SEG is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation/).

This group is focussed on Real-time collaborative editing of issue descriptions and will build upon the recent work to [view real-time updates of assignee in issue sidebar](https://gitlab.com/gitlab-org/gitlab/-/issues/17589).

The goal is to have real-time collaborative editing of issue descriptions in order to turn issues into a Google Docs type of experience.  Initially we will dogfood this at GitLab for meeting agendas and notes.

There are additional ideas within the [Real-time collaboration Epic](https://gitlab.com/groups/gitlab-org/-/epics/2345), however the goal of this SEG is the minimal functionality described above.

## Latest video

<figure class="video_container">
    <iframe width="600" height="340" src="https://www.youtube.com/embed?max-results=1&controls=0&showinfo=0&rel=0&listType=playlist&list=PL05JrBw4t0KpPmRsaVaDOoWyIp1iKacZo" frameborder="0" allowfullscreen></iframe>
</figure>

## Vision

> Make real-time collaboration on GitLab work for everything.

 You should never have to leave GitLab for pair programming, collaborating on tech designs and RFC, creating and editing issues, creating and reviewing merge requests, …

## Roadmap

1. [ ] Foundation
2. [ ] MVP: Isssue Desccriptions
3. [ ] Adoption in all editing interfaces

## Foundation

GitLabs software stack enables real-time collaboration via [Websockets](https://developer.mozilla.org/en-US/docs/Web/API/WebSocket), in particular [GraphQL subscriptions](https://graphql.org/blog/subscriptions-in-graphql-and-relay/). Real-time comes with certain complexities when users want to collaboratively edit text, in particular rich text. In order to allow multiple parties to edit the same *text* concurrently, we must ensure a conflict-free replication mode between all participating clients. There are several ways to achieve this, but we utilize [CRDTs](https://en.wikipedia.org/wiki/Conflict-free_replicated_data_type). CRDTs allow us to concurrently edit state from `1, 2, … n` clients and eventually end up with a consistent representation of the document on all clients.

Every participant in the editing process is a client (in comparison to [Operational Transformation](https://en.wikipedia.org/wiki/Operational_transformation)). The CRDT we picked ([YATA](https://www.researchgate.net/publication/310212186_Near_Real-Time_Peer-to-Peer_Shared_Editing_on_Extensible_Data_Types)) has a popular frontend implementation [Y.js](https://github.com/yjs/yjs) and we have created bindings for its [Rust](https://www.rust-lang.org/) port [`y-crdt`](https://github.com/y-crdt/y-crdt). This allows our Ruby on Rails backend to just act as one more client.

### The `y-rb` gem

**Current State:** Published to [RubyGems](https://rubygems.org/gems/y-rb).

This [Ruby](https://www.ruby-lang.org/en/) gem is developed under the `y-crdt` umbrella ([The y-crdt organization](https://github.com/y-crdt)) and brings basic and complex shared types (Array, Map, Text, XML) to Ruby. The encoded updates and state vectors are a 100% compatible with [Y.js](https://github.com/yjs/yjs) and therefore state can be synced between the JavaScript frontend and the Ruby backend in a seamless way. This allows us to e.g. add, update, or remove labels via the GitLab UI but also with background jobs (some bot adding a label concurrently).

- [Project](https://gitlab.com/gitlab-org/incubation-engineering/real-time-editing/yrb)

### Mutations over Websockets

**Current State:** Started

GitLab instantiates one or many Apollo clients on its front-end. Clients interact with the server via [links](https://www.apollographql.com/docs/react/api/link/introduction/). Those links decide which transport protocols are used for certain operations (query, mutation, or subscription). For real-time usecases, we do not only want to receive data via a persistent connection (Websocket), but also want to send changes (mutations) via the same connection. This requires some changes to our *Apollo Link* setup:

#### TODO
* Create a dedicated GraphQL endpoint for real-time use cases, or
* Rule-based splitting of links (e.g. send all mutations starting `somePrefixOp` via Websocket link)

### Initiate collaborative editing sessions

**Current State:** Not Started

For existing issues, starting a collaborative editing session should be exactly the same as the existing process. In case a second user starts to work on an issue, the backend will create a *collaborative editing session* between users and make sure that all changes (deltas) are synced between all participating parties.

#### TODO

* UX: A new issue (not "created" yet) does not have a unique ID and there is no way for us to know that people actually want to collaborate on this issue. The only way to start a session is through invitation. We can use the "assign to issue" functionality to send a notification to users, or create a unique link that can be shared with fellow editors.
* Persistence: Where to maintain a session and when to terminate. One candidate is a dedicated Redis instance.

### Editor landscape

**Current State:** Not Started

To determine and replicate a change to the description field, we need to encode everything that a user does as a change event (delta). Y.js uses a binary encoded representation to optimize payloads, but essentially it is a list of operations. One example of how this looks like is the [Quill Delta Format](https://quilljs.com/docs/delta/).

The GitLab UI does not rely on a uniform text editor but instead offeres a mix of `textarea`, `tiptap/prosemirror`, `Web IDE`, …, multiple editor modes (Raw, WYSISWYG), and post-edit transforms (Markdown parser → references). If we want to opt-in an editor UI into the collaborative editing flow, changes need to be mapped into a replication format accordingly.

#### TODO

* Use the concept of [progressive enhancement](https://developer.mozilla.org/en-US/docs/Glossary/Progressive_Enhancement). Real-time is a feature of the editor, not the product.
* Pivot to use a single editor in as many places as possible
* Move parser to frontend (WASM)

## Links

- [Epic](https://gitlab.com/groups/gitlab-org/incubation-engineering/real-time-editing/-/epics?state=opened&page=1&sort=start_date_desc)
- [Issues](https://gitlab.com/gitlab-org/incubation-engineering/real-time-editing/real-time-editing-issue-descriptions/-/issues)

### Reading List

* [Hybrid Anxiety and Hybrid Optimism: The Near Future of Work](https://future.a16z.com/hybrid-anxiety-optimism-future-of-work/)
