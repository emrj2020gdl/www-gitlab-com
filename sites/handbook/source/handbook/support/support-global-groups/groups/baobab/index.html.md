---
layout: handbook-page-toc
title: Home Page for Support's Baobab Group
description: Home Page for Support's Baobab Group
---

<!-- Search for all occurrences of NAME and replace them with the group's name.
     Search for all occurrences of URL HERE and replace them with the appropriate url -->

# Welcome to the home page of the Baobab group

Introductory text, logos, or whatever the group wants here

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Baobab resources

- Our Slack Channel: [spt_gg_baobab](https://gitlab.slack.com/archives/C03C9DU3ED8)
- Our Team: [Baobab Members](https://gitlab-com.gitlab.io/support/team/sgg.html?search=baobab)
- How to pronounce "baobab": https://www.powbab.com/blogs/news/12638601-how-do-you-pronounce-baobab-tree

## Baobab workflows and processes
